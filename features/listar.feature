#language:pt
#utf-8

Funcionalidade:Consultar Endereço
    Eu como sistema intermediário
    Quero fazer uma requisição de leitura dos dados
    Para checar o Endereço da Inmetrics

    @getBarueri
    Esquema do Cenário: Buscar Barueri
        Dado que eu tenha acesso ao webservice
        Quando eu fizer a requisiçãode leitura do Endereço
        Entao eu receba o Endereço ordenado da filial informada "<cep>"

        Exemplos:

        |   cep     |   filial     |
        | 06460000  |   são paulo  |
        | 06460000  |   barueri    |
        | 06460000  |   rio        |